/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
    View,
} from 'react-native';
import Navigation from "@navigation/index";
import {Store, applyMiddleware, createStore, compose} from 'redux';
import {IState} from "@modules/interface";
import createSagaMiddleware from '@redux-saga/core';
import logger from 'redux-logger';
import {composeWithDevTools} from "redux-devtools-extension";
import {StoreProviderService} from "./services/StoreProviderService";
import rootSaga from "@modules/saga";
import rootReducer from "@modules/reducers";
import {Provider} from "react-redux";

const configureStore = (): Store<IState> => {
    // Redux configurations
    const middleware = [];
    const enhancers = [];

    // Saga Middleware
    const sagaMiddleware = createSagaMiddleware();
    middleware.push(sagaMiddleware);

    if (__DEV__) {
        middleware.push(logger);
    }

    // Assemble Middleware
    enhancers.push(composeWithDevTools(applyMiddleware(...middleware)));

    const storeConfig: Store<IState> = createStore(rootReducer, compose(...enhancers));

    // Kick off root saga
    sagaMiddleware.run(rootSaga);

    return storeConfig;
};
StoreProviderService.init(configureStore);
const store = StoreProviderService.getStore();

class App extends React.Component<any, any> {
    render() {
        return (
            <View style={{flex: 1}}>
                <Provider store={store}>
                    <Navigation/>
                </Provider>
            </View>
        );
    }
}

export default App;
