import colors from "@styles/colors";
import {viewport} from "@styles/viewport";

const theme = {
    colors,
    viewport
}
export default theme