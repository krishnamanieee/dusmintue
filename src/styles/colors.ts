const colors = {
    primaryColor: '#00d0c0',
    secondaryColor: '#f26221',
    white: '#ffffff',
    black: '#000',
    green: '#3bd935',
    gray: 'gray'
}
export default colors;