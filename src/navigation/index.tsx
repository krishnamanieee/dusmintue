import 'react-native-gesture-handler';
import React from 'react';
import {View} from "react-native";
import {RouteNames} from "@navigation/routes";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from "@react-navigation/stack";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import Icon from 'react-native-vector-icons/Ionicons';
import HomeScreen from "../screen/HomeScreen";
import theme from "@styles/theme";
import CartScreen from "../screen/CartScreen";
import {IState} from "@modules/interface";
import {connect} from "react-redux";
import CartSelector from "@modules/cart/selectors";
import LoginScreen from "../screen/loginScreen";
import USerSelector from "@modules/user/selector";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

interface INavigationProps {
    count: number;
    isLoggedIn: boolean;
}


class Navigation extends React.Component<INavigationProps, any> {
    public render() {
        const {isLoggedIn} = this.props;
        return (
            <View style={{flex: 1}}>
                <NavigationContainer>
                    {isLoggedIn ? this.renderNavigationRoute() : this.renderAuthScreen()}
                </NavigationContainer>
            </View>
        );
    }

    private renderAuthScreen() {
        return (
            <Stack.Navigator>
                <Stack.Screen name={RouteNames.login} component={LoginScreen}
                              options={{headerShown: false}}
                />
            </Stack.Navigator>
        )
    }

    private renderNavigationRoute() {
        const {count} = this.props;
        const badge = count > 0 ? {tabBarBadge: count} : {};
        return (
            <Tab.Navigator
                screenOptions={({route}) => ({
                    tabBarIcon: ({focused, color, size}) => {
                        let iconName;

                        if (route.name === RouteNames.home) {
                            iconName = focused
                                ? 'infinite'
                                : 'infinite-outline';
                        } else if (route.name === RouteNames.events) {
                            iconName = focused ? 'calendar' : 'calendar-outline';
                        } else if (route.name === RouteNames.orders) {
                            iconName = focused ? 'basket' : 'basket-outline';
                        } else if (route.name === RouteNames.cart) {
                            iconName = focused ? 'cart' : 'cart-outline';
                        }

                        // You can return any component that you like here!
                        // @ts-ignore
                        return <Icon name={iconName} size={size} color={color}/>;
                    },
                })}
                tabBarOptions={{
                    activeTintColor: theme.colors.primaryColor,
                    inactiveTintColor: theme.colors.gray,
                }}
            >
                <Tab.Screen name={RouteNames.home} component={this.renderHome} options={{title: 'Home'}}/>
                <Tab.Screen name={RouteNames.events} component={HomeScreen} options={{title: 'Events'}}/>
                <Tab.Screen name={RouteNames.orders} component={HomeScreen} options={{title: 'Orders'}}/>
                <Tab.Screen name={RouteNames.cart} component={this.renderCart}
                            options={{title: 'Cart', ...badge}}/>
            </Tab.Navigator>
        );
    }

    renderHome() {
        return (
            <Stack.Navigator>
                <Stack.Screen name={RouteNames.homeScreen} component={HomeScreen}
                              options={{
                                  title: 'Home',
                                  headerStyle: {
                                      backgroundColor: theme.colors.primaryColor
                                  },
                                  headerTitleStyle: {
                                      color: theme.colors.white
                                  }
                              }}
                />
            </Stack.Navigator>
        )
    }

    renderCart() {
        return (
            <Stack.Navigator>
                <Stack.Screen name={RouteNames.cartScreen} component={CartScreen}
                              options={{
                                  title: 'Cart',
                                  headerStyle: {
                                      backgroundColor: theme.colors.primaryColor
                                  },
                                  headerTitleStyle: {
                                      color: theme.colors.white
                                  }
                              }}/>
            </Stack.Navigator>
        )
    }


}

const mapStateToProps = (state: IState) => {
    return {
        count: CartSelector.cartBadgeCount(state),
        isLoggedIn: USerSelector.isLoggedIn(state)
    };
};


export default connect(mapStateToProps, null)(Navigation);