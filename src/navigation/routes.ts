const RouteNames = {
    login: 'login',
    home: 'home',
    events: 'events',
    orders: 'orders',
    cart: 'cart',
    product: 'product',
    homeScreen: 'homeScreen',
    cartScreen: 'cartScreen'
}

export {
    RouteNames,
}