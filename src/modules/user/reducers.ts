import {IFluxStandardAction} from "@modules/interface";
import UserActions from "@modules/user/actions";

export interface IUserState {
    isLoggedIn: boolean;
}

const initialCartState: IUserState = {
    isLoggedIn: false,
}

/**
 * Note: we are not added original api in the POC that why it's not contain more object in reducer.
 * @param state
 * @param action
 */
const userReducers = (state: IUserState = initialCartState, action: IFluxStandardAction) => {

    switch (action.type) {
        case UserActions.userActionTypes.LOGIN.FETCH:
            return {
                ...state,
                isLoggedIn: true
            }
        default:
            return state;
    }
}
export default userReducers;