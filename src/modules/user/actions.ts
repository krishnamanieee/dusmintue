import {IFluxStandardAction} from "@modules/interface";

const actionTypePrefix = 'user/';
const userActionTypes = {
    LOGIN: {
        FETCH: `${actionTypePrefix}LOGIN`,
        SUCCESS: `${actionTypePrefix}LOGIN_SUCCESS`,
        FAILURE: `${actionTypePrefix}LOGIN_FAILURE`,
    },
}
const login = (): IFluxStandardAction => {
    return {
        type: userActionTypes.LOGIN.FETCH
    };
};

const UserActions = {
    userActionTypes,
    login,
}
export default UserActions;