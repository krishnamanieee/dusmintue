import {IState} from "@modules/interface";

const isLoggedIn = (state: IState) => {
    return state.user.isLoggedIn;
}

const USerSelector = {
    isLoggedIn,
};
export default USerSelector;