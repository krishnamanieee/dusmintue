export interface IAddToCartPayload {
    qty: number;
    id: number;
}