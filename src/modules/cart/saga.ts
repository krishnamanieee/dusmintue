import {
    addToCartFailure,
    addToCartSuccess,
    CartActionTypes, updateCartBadgeCount,
    updateCartBadgeCountFailure,
    updateCartBadgeCountSuccess
} from "@modules/cart/actions";
import {takeEvery, put, select} from "redux-saga/effects";
import {IFluxStandardAction} from "@modules/interface";
import {IAddToCartPayload} from "@modules/cart/interface";
import CartSelector from "@modules/cart/selectors";
// @ts-ignore
import response from '../../../__mockData__/productList.json';
import {findIndex, cloneDeep, map} from 'lodash';

function* addToCart(action: IFluxStandardAction<IAddToCartPayload>) {
    try {
        // @ts-ignore
        const {id, qty} = action.payload;
        const dataFromCart: any[] = yield select(CartSelector.cartProductData);
        console.log('id', id);
        console.log('qty', qty);
        console.log('dataFromCart', dataFromCart);
        const data = response.data;
        const mockDataId = findIndex(data, {'_id': id});

        const isFindId = findIndex(dataFromCart, {'_id': id});

        console.log('mockDataId', mockDataId);
        console.log('isFindId', isFindId);
        if (isFindId !== -1) {
            if (qty === 0) {
                dataFromCart.splice(isFindId, 1);
                console.log('dataFromCart', dataFromCart);
            } else {
                dataFromCart[isFindId].cartQty = qty;
            }
        } else {
            const item = data[mockDataId];
            item.cartQty = qty;
            item.isAddedInCart = true;
            dataFromCart.push(item)
        }
        yield put(updateCartBadgeCount());
        yield put(addToCartSuccess(cloneDeep(dataFromCart)));
    } catch (e) {
        yield put(addToCartSuccess((e.message)));

    }
}

function* updateCartBadge() {
    try {
        const dataFromCart: any[] = yield select(CartSelector.cartProductData);
        let count = 0;
        map(dataFromCart, (item: any) => {
            count = count + item.cartQty;
            console.log('count addded', count)
        })
        yield put(updateCartBadgeCountSuccess(count));
    } catch (e) {

        yield put(updateCartBadgeCountFailure((e.message)));
    }
}

function* watchCart() {
    yield takeEvery(CartActionTypes.ADD_TO_CART.FETCH, addToCart);
    yield takeEvery(CartActionTypes.BADGE_COUNT.FETCH, updateCartBadge);

}

export const CartSaga = {
    watchCart,
};