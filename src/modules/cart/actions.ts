import {IAddToCartPayload} from "@modules/cart/interface";
import {IFluxStandardAction} from "@modules/interface";

const actionTypePrefix = 'Cart/';
const CartActionTypes = {
    ADD_TO_CART: {
        FETCH: `${actionTypePrefix}ADD_TO_CART`,
        CLEAR: `${actionTypePrefix}ADD_TO_CLEAR`,
        SUCCESS: `${actionTypePrefix}ADD_TO_CART_SUCCESS`,
        FAILURE: `${actionTypePrefix}ADD_TO_CART_FAILURE`,
    },
    BADGE_COUNT:{
        FETCH: `${actionTypePrefix}BADGE_COUNT_CART`,
        SUCCESS: `${actionTypePrefix}BADGE_COUNT_SUCCESS`,
        FAILURE: `${actionTypePrefix}BADGE_COUNT_FAILURE`,
    }
}
const addToCart = (params: IAddToCartPayload): IFluxStandardAction<IAddToCartPayload> => {
    return {
        type: CartActionTypes.ADD_TO_CART.FETCH,
        payload: params,
    };
};

const addToCartSuccess = (data: any[]): IFluxStandardAction<any[]> => {
    return {
        type: CartActionTypes.ADD_TO_CART.SUCCESS,
        payload: data,
    };
};

const addToCartFailure = (errorMessage: string): IFluxStandardAction => {
    return {
        type: CartActionTypes.ADD_TO_CART.FAILURE,
        error: errorMessage,
    };
};

const updateCartBadgeCount = (): IFluxStandardAction => {
    return {
        type: CartActionTypes.BADGE_COUNT.FETCH,
    };
};

const updateCartBadgeCountSuccess = (count: number): IFluxStandardAction<number> => {
    return {
        type: CartActionTypes.BADGE_COUNT.SUCCESS,
        payload: count,
    };
};

const updateCartBadgeCountFailure = (errorMessage: string): IFluxStandardAction => {
    return {
        type: CartActionTypes.BADGE_COUNT.FAILURE,
        error: errorMessage,
    };
};


const clearCart = (): IFluxStandardAction<IAddToCartPayload> => {
    return {
        type: CartActionTypes.ADD_TO_CART.CLEAR
    };
};

export {
    CartActionTypes,
    addToCartFailure,
    addToCartSuccess,
    addToCart,
    clearCart,
    updateCartBadgeCount,
    updateCartBadgeCountSuccess,
    updateCartBadgeCountFailure
}