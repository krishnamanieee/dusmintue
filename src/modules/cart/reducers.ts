import {IFluxStandardAction} from "@modules/interface";
import {CartActionTypes} from "@modules/cart/actions";

export interface ICartState {
    data: any[];
    count: number;
}

const initialCartState: ICartState = {
    data: [],
    count: 0,
}

/**
 * Note: we are not added original api in the POC that why it's not contain more object in reducer.
 * @param state
 * @param action
 */
const cartReducers = (state: ICartState = initialCartState, action: IFluxStandardAction) => {

    switch (action.type) {
        case CartActionTypes.ADD_TO_CART.SUCCESS:
            return {
                ...state,
                data: action.payload
            }
        case CartActionTypes.ADD_TO_CART.CLEAR:
            return {
                ...state,
                data: []
            }
        case CartActionTypes.BADGE_COUNT.SUCCESS:
            return {
                ...state,
                count: action.payload
            }
        default:
            return state;
    }
}
export default cartReducers;