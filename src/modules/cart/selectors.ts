import {IState} from "@modules/interface";

const cartProductData = (state: IState) => {
    return state.cart.data;
}
const cartBadgeCount = (state: IState) => {
    return state.cart.count;
}

const CartSelector = {
    cartProductData,
    cartBadgeCount
};
export default CartSelector;