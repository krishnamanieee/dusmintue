import {all} from 'redux-saga/effects';
import {CartSaga} from "@modules/cart/saga";

export default function* rootSaga(): any {
    yield all([
        CartSaga.watchCart()
    ])
}