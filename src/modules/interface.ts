import {ICartState} from "@modules/cart/reducers";
import {IUserState} from "@modules/user/reducers";

export interface IFluxStandardAction<Payload = undefined, Meta = undefined, Error = string> {
    type: string;
    payload?: Payload;
    error?: Error;
    meta?: Meta;
}
export interface IState {
    cart: ICartState;
    user: IUserState;
}