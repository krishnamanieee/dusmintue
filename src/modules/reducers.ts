import {combineReducers} from 'redux';
import cartReducers from "@modules/cart/reducers";
import userReducers from "@modules/user/reducers";

// @ts-ignore
export default combineReducers({
    cart: cartReducers,
    user: userReducers
})
