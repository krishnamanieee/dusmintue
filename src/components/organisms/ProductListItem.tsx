import React from 'react';
import {Image, StyleSheet, Text, View} from "react-native";
import Button from "@components/atoms/Button";

interface IProps {
    item: any;
    index: number;
    onQtyUpdate: (qty: number, id: number)=> void;
}

class ProductListItem extends React.Component<IProps, any> {

    onPress = (qty: number) => {
        const {item, onQtyUpdate} = this.props
        onQtyUpdate(qty, item._id);
    }

    public render() {
        const {item} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.imageContainer}>
                    <Image
                        style={styles.productImg}
                        source={{uri: item.image}}
                    />
                </View>
                <View style={styles.line}/>
                <View style={styles.contentContainer}>
                    <Text style={styles.nameText}>{item.name}</Text>
                    <Text style={styles.wightText}>{item.unit}</Text>
                    <View style={styles.subContentContainer}>
                        <Text style={styles.priceText}>{'Price ₹ ' + item.priceDiscounted}</Text>
                        <View style={{alignItems: 'flex-end', flex: 1}}>
                            <Button qty={item.qty ? item.qty : 0} onPress={this.onPress}/>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

export default ProductListItem;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        margin: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        flexDirection: 'row'
    },
    line: {
        width: 1,
        backgroundColor: 'gray',
        marginHorizontal: 5,
        marginVertical: 2
    },
    imageContainer: {
        flex: 1.5,
        padding: 5,
    },
    productImg: {
        width: 100,
        height: 100,
    },
    contentContainer: {
        flex: 4,
        padding: 5,
    },
    subContentContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    nameText: {
        fontSize: 16,
        color: '#000',
    },
    priceText: {
        fontSize: 14,
        color: 'gray',
    },
    wightText: {
        fontSize: 10,
        color: 'gray',
        paddingVertical: 4,
    }
})