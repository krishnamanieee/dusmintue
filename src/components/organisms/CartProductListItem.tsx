import React from 'react';
import {Image, StyleSheet, Text, View} from "react-native";
import Button from "@components/atoms/Button";

interface IProps {
    item: any;
    index: number;
    onQtyUpdate: (qty: number, id: number)=> void;
}

class CartProductListItem extends React.Component<IProps, any> {

    onPress = (qty: number) => {
        const {item, onQtyUpdate} = this.props
        onQtyUpdate(qty, item._id);
    }
    public render() {
        const {item} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.subContentContainer}>
                    <View style={{flex: 3.5}}>
                        <Text style={styles.nameText}>{item.name}</Text>
                        <Text style={styles.wightText}>{item.unit}</Text>
                    </View>
                    <View style={{flex: 2, alignItems: 'flex-end'}}>
                        <Button qty={item.cartQty ? item.cartQty : 0} onPress={this.onPress}/>
                    </View>
                    <View style={{width: 80,marginHorizontal: 4, alignItems: 'flex-end'}}>
                        <Text style={[styles.priceText,]}> {'₹ ' + item.priceDiscounted}</Text>
                    </View>
                </View>
                <View style={styles.line}/>
            </View>
        )
    }
}

export default CartProductListItem;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 5
    },
    line: {
        height: 0.6,
        backgroundColor: 'gray',
        flex: 1,
    },
    subContentContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    nameText: {
        fontSize: 14,
        color: '#000',
    },
    priceText: {
        fontSize: 14,
        fontWeight: 'bold',
    },
    wightText: {
        fontSize: 10,
        color: 'gray',
        paddingVertical: 4,
    }
})