import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";
import theme from "@styles/theme";

interface IButtonProps {
    qty: number;
    onPress: (qty: number) => void;
}

interface IButtonState {
    displayQty: number;
}

class Button extends React.Component<IButtonProps, IButtonState> {
    state = {
        displayQty: this.props.qty
    }

    onDecPress = () => {
        const {displayQty} = this.state;
        if (displayQty > 0) {
            this.setState({
                displayQty: displayQty - 1
            }, () => {
                this.props.onPress(this.state.displayQty);
            })
        }
    }

    onInPress = () => {
        const {displayQty} = this.state;
        this.setState({
            displayQty: displayQty + 1
        }, () => {
            this.props.onPress(this.state.displayQty);
        })
    }

    render() {
        const {displayQty} = this.state;
        return displayQty === 0 ? this.renderDefaultButton() : this.renderIncDecButtonView()
    }

    renderDefaultButton() {
        return (
            <TouchableOpacity style={styles.container} onPress={this.onInPress}>
                <Text style={styles.text}>Add</Text>
            </TouchableOpacity>
        )
    }

    renderIncDecButtonView() {
        const {displayQty} = this.state;
        return (
            <View style={[styles.container, styles.multiButtonContainer]}>
                <TouchableOpacity style={styles.multiSubButtonLeft} onPress={this.onDecPress}>
                    <Text>-</Text>
                </TouchableOpacity>
                <Text style={[{paddingHorizontal: 10}]}>{displayQty}</Text>
                <TouchableOpacity style={styles.multiSubButtonRight} onPress={this.onInPress}>
                    <Text>+</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default Button;

const styles = StyleSheet.create({
    container: {
        backgroundColor: theme.colors.primaryColor,
        padding: 5,
        borderRadius: 25,
        width: 80,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    multiButtonContainer: {
        flexDirection: 'row',
        backgroundColor: theme.colors.white,
        borderColor: theme.colors.primaryColor,
        borderWidth: 1,
        padding: 0,
    },
    multiSubButtonLeft: {
        borderRightWidth: 1,
        borderColor: theme.colors.primaryColor,
        padding: 5,
    },
    multiSubButtonRight: {
        borderLeftWidth: 1,
        borderColor: theme.colors.primaryColor,
        padding: 5,
    },
    text: {
        color: theme.colors.white,
    }
})