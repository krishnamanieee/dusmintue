import React from 'react';
import {Alert, FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import theme from "@styles/theme";
import CartProductListItem from "@components/organisms/CartProductListItem";
import {IState} from "@modules/interface";
import {bindActionCreators} from "redux";
import {addToCart, clearCart} from "@modules/cart/actions";
import {connect} from "react-redux";
import CartSelector from "@modules/cart/selectors";
import {IAddToCartPayload} from "@modules/cart/interface";
import {AlertHelper} from "@utils/AlertHelper";

interface ICartScreenProps {
    data: any[];
    addToCart: (params: IAddToCartPayload) => void;
    clearCart: () => void;
}

class CartScreen extends React.Component<ICartScreenProps, any> {

    onQtyUpdate = (qty: number, id: number) => {
        this.props.addToCart({
            qty,
            id
        })
    }
    onProceedPay = () => {
        Alert.alert(
            "DusMinute",
            "Cart Checkout Successfully",
            [
                {text: "OK", onPress: this.onSuccessPress}
            ],
            {cancelable: false}
        );
    }

    onSuccessPress = () => {
        this.props.clearCart()
    }

    render() {
        const {data} = this.props;
        console.log('demo', data);
        return (
            <SafeAreaView style={{flex: 1}}>
                {
                    data.length === 0 ?
                        <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
                            <Text>Cart is Empty</Text>
                        </View>
                        :
                        <View style={{flex: 1}}>
                            {this.renderProductView()}
                            {this.renderProceedPay()}
                        </View>
                }
            </SafeAreaView>
        );
    }


    renderProductView() {
        return (
            <View style={[styles.containerProductView, styles.shadow]}>
                <View style={{justifyContent: 'space-between', flexDirection: 'row', paddingBottom: 20}}>
                    <Text style={{fontWeight: 'bold'}}>Shipment 1 of 1</Text>
                    <Text style={{color: theme.colors.green}}>Delivery by 09-08-20 </Text>
                </View>
                {this.renderProductList()}
            </View>
        )
    }

    renderProceedPay() {
        return (
            <View style={{alignItems: 'center', justifyContent: 'center', padding: 10}}>
                <TouchableOpacity
                    onPress={this.onProceedPay}
                    style={{backgroundColor: theme.colors.primaryColor, paddingHorizontal: 20, paddingVertical: 15}}>
                    <Text style={{color: theme.colors.white, fontWeight: 'bold'}}>Proceed To Pay</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderProductList() {
        const {data} = this.props;
        return (
            <FlatList
                data={data}
                renderItem={this.renderItem}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
            />
        )
    }

    renderItem = ({item, index}: any) => {
        return (
            <CartProductListItem item={item} index={index} key={index.toString()} onQtyUpdate={this.onQtyUpdate}/>
        )
    }
}

const mapStateToProps = (state: IState) => {
    return {
        data: CartSelector.cartProductData(state)
    };
};

const mapDispatchToProps = (dispatch: any) => {
    return bindActionCreators({
        addToCart,
        clearCart
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(CartScreen);

const styles = StyleSheet.create({
    shadow: {
        backgroundColor: theme.colors.white,
        margin: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        flex: 1,
    },
    containerProductView: {
        padding: 10,
    },
});