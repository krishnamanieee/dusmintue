import React from 'react';
import {Alert, Image, SafeAreaView, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import theme from "@styles/theme";
import Images from "@assets/Images";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import UserActions from "@modules/user/actions";

interface ILoginScreenProps {
    login: () => void;
}

interface ILoginScreenState {
    mobileNumber: string;
}

class LoginScreen extends React.Component<ILoginScreenProps, ILoginScreenState> {
    state = {
        mobileNumber: ''
    }
    onLoginPress = () => {
        const { mobileNumber} = this.state;
        if ( mobileNumber.length !== 10){
            Alert.alert('Enter valid the Mobile number')
            return;
        }
        this.props.login();
    }
    onInputChanges = (text: string) => {
        text = text.replace(/\D/g, '');
        this.setState({mobileNumber: text})
    }

    render() {
        console.log('theme.viewport.height', theme.viewport.height)
        return (
            <View style={styles.container}>
                <View>
                    <Image
                        style={styles.logo}
                        source={Images.logo}
                        resizeMode={"contain"}
                    />
                </View>
                <Text style={styles.loginText}>LOGIN</Text>
                <Text style={styles.descText}>Good to See You</Text>
                {this.renderInputBox()}
                {this.renderLoginButton()}
            </View>
        );
    }

    renderLoginButton() {
        return (
            <View style={{padding: 10}}>
                <TouchableOpacity
                    onPress={this.onLoginPress}
                    style={{
                        backgroundColor: theme.colors.primaryColor,
                        paddingHorizontal: 50,
                        paddingVertical: 15,
                        borderRadius: 25
                    }}>
                    <Text style={{color: theme.colors.white, fontWeight: 'bold'}}>SEND OTP</Text>
                </TouchableOpacity>
            </View>
        )
    }

    renderInputBox() {
        return (
            <View style={styles.inputContainer}>
                <Text style={{paddingHorizontal: 5}}>
                    +91
                </Text>
                <View>
                    <View style={{flex: 1, backgroundColor: theme.colors.primaryColor, width: 1}}/>
                </View>
                <TextInput
                    value={this.state.mobileNumber}
                    maxLength={10}
                    style={{padding: 10, paddingHorizontal: 20, flex: 1, fontSize: 16}}
                    keyboardType={'number-pad'}
                    onChangeText={this.onInputChanges}
                    placeholder={'Enter your Mobile number'}/>
            </View>
        )
    }
}

const mapDispatchToProps = (dispatch: any) => {
    return bindActionCreators({
        login: UserActions.login
    }, dispatch);
};

export default connect(null, mapDispatchToProps)(LoginScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: theme.colors.white,
        alignItems: 'center',
        paddingTop: ((theme.viewport.height) / 100) * 20
    },
    logo: {
        width: 200,
        height: 200,
    },
    loginText: {
        fontSize: 20,
        color: theme.colors.secondaryColor,
        fontWeight: 'bold'
    },
    descText: {
        fontSize: 16,
        color: theme.colors.gray,
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: theme.colors.primaryColor,
        borderWidth: 1,
        borderRadius: 25,
        margin: 25,
        marginHorizontal: 30,
        padding: 5,
    }
})