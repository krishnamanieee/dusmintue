import React from 'react';
import {FlatList, SafeAreaView, StatusBar, StyleSheet, Text, View} from "react-native";
// @ts-ignore
import response from '../../__mockData__/productList.json';
import ProductListItem from "@components/organisms/ProductListItem";
import {IState} from "@modules/interface";
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import {IAddToCartPayload} from "@modules/cart/interface";
import {addToCart} from "@modules/cart/actions";

interface IHoneProps {
    addToCart: (params: IAddToCartPayload) => void;
}

class HomeScreen extends React.Component<IHoneProps, any> {

    onQtyUpdate = (qty: number, id: number) => {
        this.props.addToCart({
            qty,
            id
        })
    }

    render() {
        return (
            <SafeAreaView>
                <FlatList
                    data={response.data}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                />
            </SafeAreaView>
        );
    }

    renderItem = ({item, index}: any) => {
        return (
            <ProductListItem item={item} index={index} key={index.toString()} onQtyUpdate={this.onQtyUpdate}/>
        )
    }
}

const mapStateToProps = (state: IState) => {
    return {};
};

const mapDispatchToProps = (dispatch: any) => {
    return bindActionCreators({
        addToCart
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
